/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package softdev.lab02;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author EliteCorps
 */
public class Lab02 {

    static boolean playAgain = true;
    static char[][] boardChar;
    static char turn;

    public Lab02() {
        boardChar = new char[3][3];
        turn = 'X';
        initializeBoardChar();
    }

    private static void printWelcomeGame() {
        System.out.println("<-- Welcome to Tic Tac Toe game -->");
    }
    
    private static void printThankForGame() {
        System.out.println("<-- Thanks for playing Tic Tac Toe game -->");
    }

    private static void initializeBoardChar() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                boardChar[i][j] = '-';
            }
        }
    }

    private static void printBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(boardChar[i][j] + " | ");
            }
            System.out.println("");
            System.out.println("-------------");
        }
    }

    private static void inputMove() {
        if (turn == 'X') {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please enter your move (1-9): ");
            int move = sc.nextInt();
            if (checkValidMove(move)) {
                int pRow = (move - 1) / 3;
                int pCol = (move - 1) % 3;
                boardChar[pRow][pCol] = turn;
                switchTurn();
            }
        } else {
            Random bot = new Random();
            int move;
            do {                
                move = bot.nextInt(8) + 1;
            } while (!checkValidMove(move));
            int pRow = (move - 1) / 3;
                int pCol = (move - 1) % 3;
                boardChar[pRow][pCol] = turn;
                switchTurn();
        }
    }

    private static boolean checkValidMove(int move) {
        int pRow = (move - 1) / 3;
        int pCol = (move - 1) % 3;
        if (move < 1 || move > 9) {
            if (turn != 'X') {
                return false;
            }
            System.out.println("Invalid move, please input number between 1-9");
            return false;
        }

        if (boardChar[pRow][pCol] != '-') {
            if (turn != 'X') {
                return false;
            }
            System.out.println("Invalid move, Position already taken");
            return false;
        }
        return true;
    }

    private static void printTurn() {
        System.out.println("Current turn: " + turn);
    }

    private static void switchTurn() {
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }
    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (boardChar[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Game Over!! It's a draw!!");
    }

    private static boolean checkWinner() {
        return checkRow() || checkCol() || checkDiagnol();
    }

    private static void printWinner() {
        switchTurn();
        System.out.println("Game Over!! " + turn + "'s is winner!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (boardChar[i][0] != '-' && boardChar[i][0] == boardChar[i][1] && boardChar[i][0] == boardChar[i][2]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (boardChar[0][i] != '-' && boardChar[0][i] == boardChar[1][i] && boardChar[0][i] == boardChar[2][i]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiagnol() {
        for (int i = 0; i < 3; i++) {
            if (boardChar[0][0] != '-' && boardChar[0][0] == boardChar[1][1] && boardChar[0][0] == boardChar[2][2]) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (boardChar[0][2] != '-' && boardChar[0][2] == boardChar[1][1] && boardChar[0][2] == boardChar[2][0]) {
                return true;
            }
        }
        return false;
    }

    private static void resetGame() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to play again? (Y/N): ");
        String reset = sc.next().trim().toLowerCase();
        playAgain = reset.equals("y");
    }

    public void playGame() {
        printWelcomeGame();
        while (playAgain) {
            initializeBoardChar();
            while (true) {
                printBoard();
                printTurn();
                inputMove();
                if (checkWinner()) {
                    printBoard();
                    printWinner();
                    break;
                }
                if (checkDraw()) {
                    printBoard();
                    printDraw();
                    break;
                }
            }
            resetGame();
        }
        printThankForGame();
    }

    public static void main(String[] args) {
        Lab02 game = new Lab02();
        game.playGame();
    }
}
